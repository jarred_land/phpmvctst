<?php
    class Pages extends Controller {

        public function __construct() {
            $this->people = $this->model('People');
        }

        public function index() {

            $data = [
                'title' => $this->people->title(),
                'people' => $this->people->getAllPeople()
            ];

            $this->view('pages/index',$data);
        }
/*------------------------------------------------------------------*/ 
        public function addperson() {

            $data = [
                'title' => $this->people->title(),
                'model' => $this->people->getAllPeople()
            ];

            $this->view('pages/index',$data);

        }
    }
?>