<?php

class People {

    private $db;

    public function __construct() {
        $this->db = new Database;
    }

    //function returning static data
    public function title() {
        return "Show all people";
    }

    //function returning data from database
    public function getAllPeople() {
        $this->db->query("SELECT * FROM tbl_people");
        return $this->db->resultSet();
    }

    public function addPerson($fn, $ln, $dt) {

        $this->db->query("INSERT INTO tbl_test2 (FNAME, LNAME, DOB) VALUES (:fn, :ln, :dt)");
       
        $dt = date("YYYY-mm-dd");

        $this->db->bind(":fn", $fn);
        $this->db->bind(":ln", $ln);
        $this->db->bind(":dt", $dt);

        if($this->db->execute()) {

            return true;

        } else {

            return false;

        }
    }

    
}


?>