# Task 2

In this task we are going to add a person to our page.
The exercise is this task will work synonymously for a contact or appointment form.

In this task you will also only get the required information. You will need to put the functions together yourself.

## Creating the Controller

* In Pages.php create a function for `addperson()`

* `addperson()` does not take any parameters

* The functions called in the `$data` array should be a title and a model that will add the data from a form into the database.

* The view that is called will be called `pages/addperson`

* It is best to declare the `$data` array as an empty array and add items into it.

* Remember to check if the values in the form are empty. If they have no value, only send the title to the view, not a query. You can see the implementation here: 
[https://github.com/bcsjk11/COMP6210-WK11-L01/blob/master/app/controllers/Pages.php](https://github.com/bcsjk11/COMP6210-WK11-L01/blob/master/app/controllers/Pages.php)

## Creating the Model

* In the people.php model file create a function called `addperson()` => This function takes 3 paramters. (fn, ln, date).

* In the model you need to call the query method.
* The bind method is called for each of the paramters, the date is passed in as a string. (YYYY-mm-dd) format.

* execute the query inside an if statement and return a true or false boolean that is interperate by the controller.

## Creating the View

* The View (add.php) is a new file and requires a form.

* The form needs to an action attribute (set to the url of this form, or a separate view if you like) and a method attribute needs to be set to post.

* The form needs an input field for fname, lname and a date.

* When the submit button is pressed, the next view is loaded and the data is processed. Depending on what your submit URL is, this same view might be reloaded. Make sure it is clear to the user that the data has been submitted.

* Note 1: that a form needs validation, you can use Javascript for this.

* Note 2: the header includes bootstrap, so your classes should be easy to include in the page.

* Note 3: If you are successful in this task, then creating an update and delete view should not be difficult.

