# Setup new container

Stop any current containers you have going this will free up the ports that are in use:

`docker stop $(docker ps -a)`

Navigate to your desktop (or anywhere you want to save your files)

`cd ~/Desktop`

Download the docker-compose file

`curl -LOk https://github.com/bcsjk11/LAMP-DOCKER-SETUP-72/archive/master.zip && unzip master.zip && rm -f master.zip && mv LAMP-Docker-Setup-72-master PHPWEB101`

Note you can rename PHPWEB101 into anything you want, but when you see PHPWEB101 anywhere below you will need to substitute that name with what you have chosen.

Go into the PHPWEB101 folder:

`cd PHPWEB101`

Run the docker-compose command to get your containers up and running:

`docker-compose up -d`

Go into the www folder:

`cd www`

After a new container setup has loaded, you will need to enable the rewrite mod

1: Go into the container

`docker exec -it phpweb101_web_1 bash`

2: Enable the rewrite module

`a2enmod rewrite`

3: Reload the apache2 service

`service apache2 reload`

4: Exit the container

`exit`

Download the following files into the www container (you should be in there now)

`curl -LOk https://github.com/bcsjk11/6210-MVC-Starter/archive/master.zip && unzip master.zip && rm -f master.zip`

Now you will need to go into the 6210-MVC-Starter-master folder:

`cd 6210-MVC-Starter-master`

Move all of the files into the parent folder:

`find . -maxdepth 1 -exec mv {} .. \;`

Ignore the message that says:

> mv: cannot move `.' to `../.': Device or resource busy

Go back to the parent folder and remove the 6210... folder

`cd .. && rm -rf 6210-MVC-Starter-master`

Create your git repository:

`git init`

Add your assignment repo link

`git remote add origin https://url-to-your-repo` (where "https://url-to-your-repo" is the link to your github or bitbucket repository)

To make the url redirection work correctly you will need to enable the rewrite module in the container.

```
docker exec -it PHPWEB101 bash
```

You are now in the container and should see root@ at the beginning of the promt line

```
a2enmod rewrite
service apache2 reload
```

Exit the container by type in `exit` followed by <ENTER>

Now you can go to localhost:8000 (mac and linux) or 192.168.99.100:8000 (windows when using docker toolbox) and you will see the following message:

> Fatal error: Uncaught Error: Class 'Pages' not found in /var/www/html/app/libraries/Core.php:21 Stack trace: #0 /var/www/html/public/index.php(4): Core->__construct() #1 {main} thrown in /var/www/html/app/libraries/Core.php on line 21


---

