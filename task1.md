# Pre Read

To use the MVC pattern, you need to think about what it is you want to create and split it up in the relevant parts

* Controller => get and send the request. Collect information from the Model (ie database) and select the view this information needs to be presented in.

* View => Mostly container HTML code. Any PHP code is only there to display the data send from the controller

* Model => Makes the queries to the database and send the information back to the controller, so that it can be passed to the View.

## **THE MODEL NEVER TALKS TO THE VIEW DIRECTLY!**

# Task 1

This framework comes by default with a pages controller that has an index method. 

These will respond to the default parameters set in the `Controller.php` file.

From the controller we need to link to a view and a model.

## Connect to the database

Create the database from `part1.sql`
This will allow use to show some data on our pages.

Check that the constants in the config.php file (helper folder) to this

```
define ("DBHOST", "phpweb101_db_1");
define ("DBUSER", "root");
define ("DBPASS", "(password123)");
define ("DBNAME", "containerdb");
```

## Creating the Controller

First in the Pages.php controller file add this code:

````
class Pages extends Controller {

    public function __construct() {
        $this->people = $this->model('People');
    }

    public function index() {

        $data = [
            'title' => $this->people->title(),
            'people' => $this->people->getAllPeople()
        ];

        $this->view('pages/index',$data);
    }
}
````

As you can see here the `__construct()` function sets up the model. This points to the model file, so all of the methods inside of the people model are linked in through this variable.

* They **model** functions are called and set in the data array.
* The data array is then passed into the **view**.

## Creating the model 

After we create the controller, we need to create the model. The data can be static or directly from a database. Static data could be placed in the Controller, but the best place is the model.

Create a file in models folder call it People.php

Place this code in it:

````

class People {

    private $db;

    public function __construct() {
        $this->db = new Database;
    }

    //function returning static data
    public function title() {
        return "Show all people";
    }

    //function returning data from database
    public function getAllPeople() {
        $this->db->query("SELECT * FROM tbl_people");
        return $this->db->resultSet();
    }
}

````

The variable and the constructor must be placed in each of the model class' that you create.
The methods depend on wether you need them.

As you can see we have one method that returns a string and another that returns an array from a query.

## Creating the view

The view can be really small or really big (code wise) it all depends on the data that is meant to be displayed.

Copy this code into the index.php in the view/Pages folder.

````

<?php require APPROOT . "/views/includes/header.php"; ?>

    <div class="row">
        <div class="col-sm-12">
        <h1><?php echo $data['title']; ?></h1>
            <table class="table">
                <thead class="myheader">
                    <tr>
                        <th>Name</th>
                        <th>Company Name</th>
                        <th>Age</th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                        $output = "";

                        $today = new DateTime('now');
                        $todayPrint = $today->format('d/m/Y');

                        foreach($data['people'] as $item) {

                            $date = new DateTime($item['DOB']);
                            $datePrint = $date->format('d/m/Y');

                            $diff = date_diff($today, $date);
                            $diff = $diff->format('%y Years');

                            $output .= "<tr><td>";
                            $output .= $item['FNAME'];
                            $output .= " ";
                            $output .= $item['LNAME'];
                            $output .= "</td><td>";
                            $output .= $datePrint;
                            $output .= "</td><td>";
                            $output .= $diff;
                            $output .= "</td></tr>";
                        }

                        echo $output;
                    ?>
                </tbody>
            </table>
        </div>
    </div>

<?php require APPROOT . "/views/includes/footer.php"; ?>

```` 

## Putting it all together

Now you have put together a webpage.

You will see that the model does talk to the view and the view is not talking to the model. Everything comes in and is processed in the controller.

You should now be able to go to http://localhost:8000 or http://192.168.99.100:8000 and see the page.


